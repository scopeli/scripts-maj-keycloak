package fr.scopeli.keycloakhelper.userloader;

import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AddDefaultGroupOldUsers {
	
	private static Logger LOGGER = LoggerFactory.getLogger(AddDefaultGroupOldUsers.class);

	
	public static void main(String [] args) {
		String serverUrl = args[0]; //"https://annuaire.scopeli.fr/auth";
		String realm = args[1]; //"realmname";
		// idm-client needs to allow "Direct Access Grants: Resource Owner Password
		// Credentials Grant"
		String clientId = args[2]; //"fr.scopeli.keycloahelper.userloader";
		String clientSecret = args[3]; //"XXXXXXXXXX";
		String userAdminName = args[4]; //"nom.prenom@provider.fr";
		String userAdminPassword = args[5]; //"YYYYYYYYYYYY";
		
		KeycloakUtils keycloakutils = new KeycloakUtils(serverUrl, realm, clientId, clientSecret, userAdminName, userAdminPassword);
		int nbAccounts = keycloakutils.getUsersResource().count();
		LOGGER.info("Nombres de comptes déclarés dans le realm Scopéli : " + nbAccounts);

		//for (UserRepresentation user : keycloakutils.getUsersResource().list(0,4)) {
			
		for (UserRepresentation user : keycloakutils.getUsersResource().list(0,nbAccounts-1)) {
			
			keycloakutils.addGroupsMenagesPolyvolant(user.getId());
			LOGGER.info("Ajout des groupes par défaut de {} {} ({})",user.getFirstName(),user.getLastName(),user.getUsername());
		}
		
		
	}

}
