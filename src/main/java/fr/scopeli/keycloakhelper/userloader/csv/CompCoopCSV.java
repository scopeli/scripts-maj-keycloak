package fr.scopeli.keycloakhelper.userloader.csv;

import com.opencsv.bean.CsvBindByPosition;

public class CompCoopCSV {
	
	@CsvBindByPosition(position = 0)
	private String numeroCoop;
	
	@CsvBindByPosition(position = 1)
	private String  nom;
	
	@CsvBindByPosition(position = 2)
	private String prenom;
	
	
	@CsvBindByPosition(position = 5)
	private String nbOccurence;
	
	@CsvBindByPosition(position = 3)
	private String formations;
	
	@CsvBindByPosition(position = 4)
	private String mail;

	public String getNumeroCoop() {
		return numeroCoop;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}


	public String getNbOccurence() {
		return nbOccurence;
	}

	public String getFormations() {
		return formations;
	}

	public String getMail() {
		return mail;
	}

	@Override
	public String toString() {
		return "CompCoopCSV [numeroCoop=" + numeroCoop + ", nom=" + nom + ", prenom=" + prenom 
				+ ", nbOccurence=" + nbOccurence + ", formations=" + formations + ", mail=" + mail + "]";
	}

	
}
