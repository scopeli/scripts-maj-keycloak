package fr.scopeli.keycloakhelper.userloader.csv;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class CoopCSV {

	@CsvBindByPosition(position = 0)
	//@CsvBindByName(column = "Code Kaso")
	private String matriculeKaso;
	
	@CsvBindByPosition(position = 5)
	//@CsvBindByName(column = "Matricule")
	private String matriculeCoop;	
	
	@CsvBindByPosition(position = 6)
	//@CsvBindByName(column = "Nom")
	private String  nom;
	
	@CsvBindByPosition(position = 7)
	//@CsvBindByName(column = "Pr�nom")
	private String prenom;
	
	@CsvBindByPosition(position = 9)
	//@CsvBindByName(column = "Adresse")
	private String adresse;
	
	@CsvBindByPosition(position = 10)
	//@CsvBindByName(column = "Compl�ment d�adresse")
	private String complement;
	
	@CsvBindByPosition(position = 11)
	//@CsvBindByName(column = "CP")
	private String cp;
	
	@CsvBindByPosition(position = 12)
	//@CsvBindByName(column = "VILLE (MAJUSCULE)")
	private String ville;
	
	@CsvBindByPosition(position = 13)
	//@CsvBindByName(column = "T�l�phone")
	private String telephone;
	
	@CsvBindByPosition(position = 14)
	//@CsvBindByName(column = "Mail")
	private String mail;
	
	@CsvBindByPosition(position = 15)
	//@CsvBindByName(column = "Statut coop")
	private String statut;
	
	@CsvBindByPosition(position = 4)
	//@CsvBindByName(column = "Formule Duo")
	private String matriculeDuo;

	public String getMatriculeCoop() {
		return matriculeCoop;
	}

	public void setMatriculeCoop(String matriculeCoop) {
		this.matriculeCoop = matriculeCoop;
	}

	public String getMatriculeKaso() {
		return matriculeKaso;
	}

	public void setMatriculeKaso(String matriculeKaso) {
		this.matriculeKaso = matriculeKaso;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public String getComplement() {
		return complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}

	public String getMatriculeDuo() {
		return matriculeDuo;
	}

	public void setMatriculeDuo(String matriculeDuo) {
		this.matriculeDuo = matriculeDuo;
	}

	@Override
	public String toString() {
		return "CoopCSV [matriculeKaso=" + matriculeKaso + ", matriculeCoop=" + matriculeCoop + ", nom=" + nom
				+ ", prenom=" + prenom + ", adresse=" + adresse + ", complement=" + complement + ", cp=" + cp
				+ ", ville=" + ville + ", telephone=" + telephone + ", mail=" + mail + ", statut=" + statut
				+ ", matriculeDuo=" + matriculeDuo + "]";
	}

	
	
}
