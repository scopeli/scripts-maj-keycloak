package fr.scopeli.keycloakhelper.userloader;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ws.rs.core.Response;

import org.apache.commons.collections.map.HashedMap;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.GroupsResource;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.scopeli.keycloakhelper.userloader.csv.CoopCSV;

public class KeycloakUtils {

	private static final String ATTRIBUTENAME_MATRICULE_KASO = "code_kaso";

	private static final String ATTRIBUTENAME_MATRICULE_SCOPELI = "matricule";

	private static final String ATTRIBUTENAME_LOCALE = "locale";

	private static final String ATTRIBUTENAME_TELEPHONE = "telephone";

	private static final String ATTRIBUTENAME_ADRESSE_VILLE = "adresse.ville";

	private static final String ATTRIBUTENAME_ADRESSE_CODE_POSTAL = "adresse.code_postal";

	private static final String ATTRIBUTENAME_ADRESSE_COMPLEMENT = "adresse.complement";

	private static final String ATTRIBUTENAME_ADRESSE_ADRESSE = "adresse.adresse";

	private static final String ATTRIBUTENAME_FULLNAME = "fullName";

	private static final String ATTRIBUTENAME_ID = "id";

	private static final String ATTRIBUTENAME_STATUT = "statut";

	private static final String ATTRIBUTENAME_CONJOINT = "conjoint";

	private static final String ATTRIBUTENAME_PROFESSION = "profession";

	private static final String ATTRIBUTENAME_GENRE = "genre";

	private static final String ATTRIBUTENAME_DATE_NAISSANCE = "date_naissance";

	private static final String ATTRIBUTENAME_CO_COOP = "cocoop_matricule";

	private static Logger LOGGER = LoggerFactory.getLogger(KeycloakUtils.class);

	private static Logger LOGGER_CR = LoggerFactory.getLogger("CR");

	private static Logger LOGGER_ERROR = LoggerFactory.getLogger("ERROR");

	private Keycloak keycloak;

	private RealmResource realmResource;

	private GroupRepresentation menageGroup = null;
	private GroupRepresentation polyvolantGroup = null;

	private UsersResource usersResource = null;

	private Map<String, GroupRepresentation> groupByFormationName = new HashedMap();

	public KeycloakUtils(String serverUrl, String realm, String clientId, String clientSecret, String userAdminName,
			String userAdminPassword) {

		this.keycloak = KeycloakBuilder.builder() //
				.serverUrl(serverUrl) //
				.realm(realm) //
				.grantType(OAuth2Constants.PASSWORD) //
				.clientId(clientId) //
				.clientSecret(clientSecret) //
				.username(userAdminName) //
				.password(userAdminPassword) //
				.build();

		this.realmResource = keycloak.realm(realm);

	}

	public Keycloak getKeycloak() {
		return keycloak;
	}

	public RealmResource getRealmResource() {
		return realmResource;
	}

	public GroupsResource getGroupsResource() {
		return realmResource.groups();
	}

	public UsersResource getUsersResource() {
		if (this.usersResource == null) {
			this.usersResource = realmResource.users();
		}
		return this.usersResource;
	}

	public void addGroupsMenagesPolyvolant(String userId) {
		if (this.menageGroup == null) {
			this.menageGroup = realmResource.getGroupByPath("/infrastructure/informatique/elefan/formations/ménage");
		}
		if (this.polyvolantGroup == null) {
			this.polyvolantGroup = realmResource
					.getGroupByPath("/infrastructure/informatique/elefan/formations/polyvolant");
		}
		LOGGER.debug("Recherche de l'utilisateur d'Id {}", userId);
		UserResource userResource = getUsersResource().get(userId);
		LOGGER.debug("Ajout des groupes {} et {}", menageGroup, polyvolantGroup);
		userResource.joinGroup(menageGroup.getId());
		userResource.joinGroup(polyvolantGroup.getId());

	}

	public void addGroupByFormationName(UserRepresentation userToUpdate, String formationName, boolean fake) {
		GroupRepresentation formationGroup = getGroupFromFormationName(formationName);
		if (!fake && formationGroup != null) {
			LOGGER.info("Ajout de la formation {} a {} {} ({})", formationName, userToUpdate.getFirstName(),
					userToUpdate.getLastName(), userToUpdate.getId());
			LOGGER_CR.info("{};{};{};{}", userToUpdate.getEmail(), userToUpdate.getFirstName(),
					userToUpdate.getLastName(), formationName);
			UserResource userResource = getUsersResource().get(userToUpdate.getId());
			userResource.joinGroup(formationGroup.getId());
		} else if (fake) {
			LOGGER.info("FAKE : Ajout de la formation {} a {} {} ({})", formationName, userToUpdate.getFirstName(),
					userToUpdate.getLastName(), userToUpdate.getId());
			LOGGER_CR.info("FAKE : {};{};{};{}", userToUpdate.getEmail(), userToUpdate.getFirstName(),
					userToUpdate.getLastName(), formationName);
		}

	}
	
	public void addGroupByGroupPath(UserRepresentation userToUpdate, String groupPath, boolean fake) {
		GroupRepresentation group = realmResource.getGroupByPath(groupPath);
		if (!fake && group != null) {
			LOGGER.info("Ajout du groupe {} a {} {} ({})", groupPath, userToUpdate.getFirstName(),
					userToUpdate.getLastName(), userToUpdate.getId());
			LOGGER_CR.info("{};{};{};+{}", userToUpdate.getEmail(), userToUpdate.getFirstName(),
					userToUpdate.getLastName(), groupPath);
			UserResource userResource = getUsersResource().get(userToUpdate.getId());
			userResource.joinGroup(group.getId());
		} else if (fake) {
			LOGGER.info("FAKE : Ajout du groupe {} a {} {} ({})", groupPath, userToUpdate.getFirstName(),
					userToUpdate.getLastName(), userToUpdate.getId());
			LOGGER_CR.info("FAKE : {};{};{};+{}", userToUpdate.getEmail(), userToUpdate.getFirstName(),
					userToUpdate.getLastName(), groupPath);
		}

	}
	
	public void removeGroupByGroupPath(UserRepresentation userToUpdate, String groupPath, boolean fake) {
		GroupRepresentation group = realmResource.getGroupByPath(groupPath);
		if (!fake && group != null) {
			LOGGER.info("Retrait du groupe {} a {} {} ({})", groupPath, userToUpdate.getFirstName(),
					userToUpdate.getLastName(), userToUpdate.getId());
			LOGGER_CR.info("{};{};{};-{}", userToUpdate.getEmail(), userToUpdate.getFirstName(),
					userToUpdate.getLastName(), groupPath);
			UserResource userResource = getUsersResource().get(userToUpdate.getId());
			userResource.leaveGroup(group.getId());
		} else if (fake) {
			LOGGER.info("FAKE : Retrait du groupe {} a {} {} ({})", groupPath, userToUpdate.getFirstName(),
					userToUpdate.getLastName(), userToUpdate.getId());
			LOGGER_CR.info("FAKE : {};{};{};-{}", userToUpdate.getEmail(), userToUpdate.getFirstName(),
					userToUpdate.getLastName(), groupPath);
		}

	}


	public GroupRepresentation getGroupFromFormationName(String formationName) {
		LOGGER.debug("Recherche de la formation {}", formationName);

		if ("Poly-volant".equals(formationName) || "".equals(formationName.trim()))
			return null;

		GroupRepresentation founded = groupByFormationName.get(formationName);
		if (founded == null) {
			founded = realmResource.getGroupByPath(getPathForFormation(formationName));
			if (founded == null) {
				LOGGER.error("La formation {} n'a pas été trouvée !", formationName);
				System.exit(1);
			} else {
				groupByFormationName.putIfAbsent(formationName, founded);
			}
			return founded;
		} else
			return founded;

	}
	
	

	private String getPathForFormation(String formationName) {

		/*
		 * 'Accueil magasin':
		 * /infrastructure/informatique/elefan/formations/accueil_magasin Caisse:
		 * /infrastructure/informatique/elefan/formations/caisse Ouverture:
		 * /infrastructure/informatique/elefan/formations/ouverture Poly-volant:
		 * /infrastructure/informatique/elefan/formations/polyvolant Ménage:
		 * /infrastructure/informatique/elefan/formations/ménage 'Accueil
		 * équipe/Appui-coops':
		 * /infrastructure/informatique/elefan/formations/appui-coop
		 * Fromagerie/Boucherie:
		 * /infrastructure/informatique/elefan/formations/fromagerie-boucherie Primeurs:
		 * /infrastructure/informatique/elefan/formations/primeurs Vrac:
		 * /infrastructure/informatique/elefan/formations/vrac Stock:
		 * /infrastructure/informatique/elefan/formations/stock Fermeture:
		 * /infrastructure/informatique/elefan/formations/fermeture
		 */

		switch (formationName) {
		case "Primeurs":
			return "/infrastructure/informatique/elefan/formations/primeurs";
		case "Fromagerie/Boucherie":
			return "/infrastructure/informatique/elefan/formations/fromagerie-boucherie";
		case "Accueil magasin":
			return "/infrastructure/informatique/elefan/formations/accueil_magasin";
		case "Vrac":
			return "/infrastructure/informatique/elefan/formations/vrac";
		case "Caisse":
			return "/infrastructure/informatique/elefan/formations/caisse";
		case "Stock":
			return "/infrastructure/informatique/elefan/formations/stock";
		case "Fermeture":
			return "/infrastructure/informatique/elefan/formations/fermeture";
		case "Ouverture":
			return "/infrastructure/informatique/elefan/formations/ouverture";
		case "Accueil équipe/Appui-coops":
			return "/infrastructure/informatique/elefan/formations/appui-coop";
		case "Accueil �quipe/Appui-coops":
			return "/infrastructure/informatique/elefan/formations/appui-coop";
		default:
			return "NOTFOUND";
		}
	}

	public void createUser(CoopCSV coop, boolean fake, boolean sendEmail) {
		UserRepresentation user = new UserRepresentation();
		user.setEmailVerified(false);
		user.setEnabled(true);
		user.setRequiredActions(Collections.singletonList("UPDATE_PASSWORD"));
		user.setUsername(coop.getMail().trim());
		user.setFirstName(coop.getPrenom().trim());
		user.setLastName(coop.getNom().trim());
		user.setEmail(coop.getMail().trim());

		Map<String, List<String>> attributes = Collections.synchronizedMap(new HashMap<String, List<String>>());
		user.setAttributes(attributes);

		updatesUsersttributes(coop, attributes);

		LOGGER.debug("Creation of User {} with attributes {} ", user, attributes);

		if (!fake) {
			Response response = getUsersResource().create(user);
			LOGGER.debug("Repsonse: {} {}", response.getStatus(), response.getStatusInfo());
			LOGGER.debug("Location : {}", response.getLocation());
			String userId = CreatedResponseUtil.getCreatedId(response);
			LOGGER.info("UserId : {}", userId);

			if (sendEmail) {
				// 1296000 c'est la durée de validité du lien permettant de définir le mot de passe
				UserResource userRessource = this.getRealmResource().users().get(userId);
				userRessource.executeActionsEmail(Collections.singletonList("UPDATE_PASSWORD"), 1296000);
				LOGGER.info("UserId : {} -> Email sent", userId);
			}
		}

	}

	private void updatesUsersttributes(CoopCSV coop, Map<String, List<String>> attributes) {
		attributes.put(ATTRIBUTENAME_ADRESSE_ADRESSE, Collections.singletonList(coop.getAdresse()));
		attributes.put(ATTRIBUTENAME_ADRESSE_COMPLEMENT, Collections.singletonList(coop.getComplement()));
		attributes.put(ATTRIBUTENAME_ADRESSE_CODE_POSTAL, Collections.singletonList(coop.getCp()));
		attributes.put(ATTRIBUTENAME_ADRESSE_VILLE, Collections.singletonList(coop.getVille()));
		attributes.put(ATTRIBUTENAME_TELEPHONE, Collections.singletonList(coop.getTelephone()));
		attributes.put(ATTRIBUTENAME_LOCALE, Collections.singletonList("fr"));
		attributes.put(ATTRIBUTENAME_ID, Collections.singletonList(coop.getMatriculeCoop()));
		attributes.put(ATTRIBUTENAME_MATRICULE_SCOPELI, Collections.singletonList(coop.getMatriculeCoop().trim()));
		attributes.put(ATTRIBUTENAME_MATRICULE_KASO, Collections.singletonList(coop.getMatriculeKaso().trim()));
		attributes.put(ATTRIBUTENAME_FULLNAME, Collections.singletonList(coop.getPrenom() + " " + coop.getNom()));
		if (!coop.getMatriculeDuo().isBlank()) {
			attributes.put(ATTRIBUTENAME_CO_COOP, Collections.singletonList(coop.getMatriculeDuo()));

		}
	}

	public void updateUser(UserRepresentation userToUpdate, CoopCSV coop, boolean fake) {

		// Retrait des attributs superflux
		userToUpdate.getAttributes().remove(ATTRIBUTENAME_CONJOINT);
		userToUpdate.getAttributes().remove(ATTRIBUTENAME_GENRE);
		// userToUpdate.getAttributes().remove(ATTRIBUTENAME_ID);
		userToUpdate.getAttributes().remove(ATTRIBUTENAME_DATE_NAISSANCE);
		userToUpdate.getAttributes().remove(ATTRIBUTENAME_STATUT);
		userToUpdate.getAttributes().remove(ATTRIBUTENAME_PROFESSION);

		updatesUsersttributes(coop, userToUpdate.getAttributes());

		if (!fake) {
			UserResource userRessource = this.getRealmResource().users().get(userToUpdate.getId());

			// update FirstName LastName
			userToUpdate.setFirstName(coop.getPrenom());
			userToUpdate.setLastName(coop.getNom());

			userRessource.update(userToUpdate);
		}

	}

	public UserRepresentation findByUserNameOrMatricule(String mail, String matricule)
			throws InvalidMatriculeException {

		List<UserRepresentation> founded = this.getUsersResource().search(mail);
		if (!founded.isEmpty() && founded.size() == 1) {
			LOGGER.debug("User {} trouvé par email", mail);
			UserRepresentation foundedRep = founded.get(0);
			String foundedMatricule = null;
			if (foundedRep.getAttributes().containsKey(ATTRIBUTENAME_ID)) {
				foundedMatricule = foundedRep.getAttributes().get(ATTRIBUTENAME_ID).get(0);
			}
			if (foundedRep.getAttributes().containsKey(ATTRIBUTENAME_MATRICULE_SCOPELI)) {
				foundedMatricule = foundedRep.getAttributes().get(ATTRIBUTENAME_MATRICULE_SCOPELI).get(0);
			}
			// Si le matricule trouvé ne correspond pas au matricule attendu
			if (matricule != null && !matricule.equals(foundedMatricule)) {
				InvalidMatriculeException e = new InvalidMatriculeException();
				e.foundedMatricule = foundedMatricule;
				throw e;
			}
			return foundedRep;
		} else {
			founded = this.getRealmResource().users().searchByAttributes(ATTRIBUTENAME_ID + ":" + matricule);
			if (!founded.isEmpty() && founded.size() == 1) {
				LOGGER.debug("User {} trouv� par attribut {}", mail, ATTRIBUTENAME_ID);
				return founded.get(0);
			} else {
				founded = this.getRealmResource().users()
						.searchByAttributes(ATTRIBUTENAME_MATRICULE_SCOPELI + ":" + matricule);
				if (!founded.isEmpty() && founded.size() == 1) {
					LOGGER.debug("User {} trouvé par attribut {}", mail, ATTRIBUTENAME_MATRICULE_SCOPELI);
					return founded.get(0);
				}
			}
		}
		return null;
	}

	public void desactiveUser(UserRepresentation userToDesactivate, CoopCSV coop, boolean fake) {
		userToDesactivate.setEnabled(false);
		if (!fake) {
			UserResource userRessource = this.getRealmResource().users().get(userToDesactivate.getId());
			userRessource.update(userToDesactivate);
		}
	}
	
	public List<String> listGroups(UserRepresentation user) {
		UserResource userResource = getUsersResource().get(user.getId());
		return userResource.groups().stream()
			    .map(GroupRepresentation::getPath)
			    .collect(Collectors.toList());
	}
}
