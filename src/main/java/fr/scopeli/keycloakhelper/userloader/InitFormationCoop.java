package fr.scopeli.keycloakhelper.userloader;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.bean.CsvToBeanBuilder;

import fr.scopeli.keycloakhelper.userloader.csv.CompCoopCSV;


public class InitFormationCoop {

	private static Logger LOGGER = LoggerFactory.getLogger(InitFormationCoop.class);

	private static Logger LOGGER_ERROR = LoggerFactory.getLogger("ERROR");
	
	
	public static void main(String[] args) throws IllegalStateException, FileNotFoundException, UnsupportedEncodingException {

		String serverUrl = args[0]; // "https://annuaire.scopeli.fr/auth";
		String realm = args[1]; // "realmname";
		// idm-client needs to allow "Direct Access Grants: Resource Owner Password
		// Credentials Grant"
		String clientId = args[2]; // "fr.scopeli.keycloahelper.userloader";
		String clientSecret = args[3]; // "XXXXXXXXXX";
		String userAdminName = args[4]; // "nom.prenom@provider.fr";
		String userAdminPassword = args[5]; // "YYYYYYYYYYYY";

		String filename = args[6]; // "C:\\Users\\sebbi\\eclipse-workspace\\userloader\\src\\main\\resources\\CompetencesCooperateurs18092023.csv";

		KeycloakUtils keycloakutils = new KeycloakUtils(serverUrl, realm, clientId, clientSecret, userAdminName,
				userAdminPassword);
		int nbAccounts = keycloakutils.getUsersResource().count();
		LOGGER.info("Nombres de comptes déclarés dans le realm Scopéli : " + nbAccounts);

		
		//////////////////// Changer ici pour tester ou pas 
		boolean fake = true;
		////////////////////
		
		
		String fakeString = (fake)?"FAKE: ":"";		
		List<CompCoopCSV> beans = new CsvToBeanBuilder<CompCoopCSV>(new InputStreamReader(new FileInputStream(filename), "UTF-8")).withType(CompCoopCSV.class)
				.withSeparator(';').withSkipLines(1).withIgnoreEmptyLine(true).withQuoteChar('"').build().parse();


		beans.forEach(compcoop -> {
			LOGGER.info("Traitement du cooperateur {} - {} {} ({})", compcoop.getNumeroCoop(), compcoop.getPrenom(), compcoop.getNom(),
					compcoop.getMail());
			if (compcoop.getMail() != null) {
				UserRepresentation founded;
				try {
					founded = keycloakutils.findByUserNameOrMatricule(compcoop.getMail(), compcoop.getNumeroCoop());
				
					if (founded != null) {
						
						LOGGER.debug("Utilisateur {} trouvé", founded.getId());					
						keycloakutils.addGroupByFormationName(founded, compcoop.getFormations(), fake);
						
					} else {
						LOGGER.warn("Utilisateur {} non trouvé", compcoop.getMail());
					}
				
				} catch (InvalidMatriculeException e) {
					LOGGER.warn("Les matricules et email de l'utilisateurs ne correspondent pas {} / {}", compcoop.getNumeroCoop(), compcoop.getMail());
					
					LOGGER_ERROR.info("{}{};{};{};Utilisateur {} trouvé par email, mais matricule incorrect (Fichier : {} - Keycloak : {})",
							fakeString, compcoop.getMail(), compcoop.getPrenom(), compcoop.getNom(), compcoop.getMail(), compcoop.getNumeroCoop(), e.foundedMatricule);
				}
				
			} else {
				LOGGER.warn("Utilisateur de matricule {} non trouvé", compcoop.getNumeroCoop());
			}
		});

	}

}
