/**
 * 
 */
package fr.scopeli.keycloakhelper.userloader;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.TreeSet;

import javax.ws.rs.WebApplicationException;

import org.checkerframework.checker.units.qual.g;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.bean.CsvToBeanBuilder;

import fr.scopeli.keycloakhelper.userloader.csv.CoopCSV;

/**
 * @author sebbi
 *
 */
public class AddUserToGroup {

	private static Logger LOGGER = LoggerFactory.getLogger(AddUserToGroup.class);

	private static Logger LOGGER_CR = LoggerFactory.getLogger("CR");

	private static Logger LOGGER_ERROR = LoggerFactory.getLogger("ERROR");

	private static Logger LOGGER_WARNING = LoggerFactory.getLogger("WARNING");

	private static Logger LOGGER_DUO = LoggerFactory.getLogger("DUO");

	/**
	 * @param args
	 * @throws FileNotFoundException
	 * @throws IllegalStateException
	 * @throws UnsupportedEncodingException
	 */
	public static void main(String[] args)
			throws IllegalStateException, FileNotFoundException, UnsupportedEncodingException {
		String serverUrl = args[0]; // "https://annuaire.scopeli.fr/auth";
		String realm = args[1]; // "realmname";
		// idm-client needs to allow "Direct Access Grants: Resource Owner Password
		// Credentials Grant"
		String clientId = args[2]; // "fr.scopeli.keycloahelper.userloader";
		String clientSecret = args[3]; // "XXXXXXXXXX";
		String userAdminName = args[4]; // "nom.prenom@provider.fr";
		String userAdminPassword = args[5]; // "YYYYYYYYYYYY";
		
		String userMail = "ACOMPLETER";
		String userMatricule = "ACOMPLETER";
		String groupToAdd = "ACOMPLETER";
		

		KeycloakUtils keycloakutils = new KeycloakUtils(serverUrl, realm, clientId, clientSecret, userAdminName,
				userAdminPassword);

		UserRepresentation founded;
		try {
			founded = keycloakutils.findByUserNameOrMatricule(userMail, userMatricule);

			if (founded != null) {

				LOGGER.info("Utilisateur {}/{} trouvé", userMail,userMatricule);
				
				List<String> allUserGroups = keycloakutils.listGroups(founded);
				
				if (allUserGroups.contains(groupToAdd)) {
					LOGGER.warn("l'utilisateur {} possède déjà le groupe {}",userMail,groupToAdd);
				} else {
					
					addGroup(keycloakutils,founded,allUserGroups,groupToAdd);
				}
				
			}
		} catch (InvalidMatriculeException e) {

			LOGGER_ERROR.info("Utilisateur non trouvé");

		}

	}

	private static void addGroup(KeycloakUtils keycloakutils, UserRepresentation user, List<String> allUserGroups, String groupToAdd) {
		TreeSet<String> groupToManage = new TreeSet<String>();
		for(String group : allUserGroups) {			
			if(!group.equals(groupToAdd) && group.contains(groupToAdd)) {
				groupToManage.add(group);
			}
		}
		
		LOGGER.info("Groups to add/remove {}",groupToManage);
		for(String path : groupToManage) {
			keycloakutils.removeGroupByGroupPath(user, path, false);
		}
		keycloakutils.addGroupByGroupPath(user, groupToAdd, false);
		for(String path : groupToManage) {
			keycloakutils.addGroupByGroupPath(user, path, false);
		}
		
	}
}
