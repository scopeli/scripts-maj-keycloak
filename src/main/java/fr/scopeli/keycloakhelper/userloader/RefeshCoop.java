/**
 * 
 */
package fr.scopeli.keycloakhelper.userloader;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.ws.rs.WebApplicationException;

import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.bean.CsvToBeanBuilder;

import fr.scopeli.keycloakhelper.userloader.csv.CoopCSV;

/**
 * @author sebbi
 *
 */
public class RefeshCoop {

	private static Logger LOGGER = LoggerFactory.getLogger(RefeshCoop.class);

	private static Logger LOGGER_CR = LoggerFactory.getLogger("CR");

	private static Logger LOGGER_ERROR = LoggerFactory.getLogger("ERROR");
	
	private static Logger LOGGER_WARNING = LoggerFactory.getLogger("WARNING");
	
	private static Logger LOGGER_DUO = LoggerFactory.getLogger("DUO");


	/**
	 * @param args
	 * @throws FileNotFoundException
	 * @throws IllegalStateException
	 * @throws UnsupportedEncodingException 
	 */
	public static void main(String[] args) throws IllegalStateException, FileNotFoundException, UnsupportedEncodingException {
		String serverUrl = args[0]; // "https://annuaire.scopeli.fr/auth";
		String realm = args[1]; // "realmname";
		// idm-client needs to allow "Direct Access Grants: Resource Owner Password
		// Credentials Grant"
		String clientId = args[2]; // "fr.scopeli.keycloahelper.userloader";
		String clientSecret = args[3]; // "XXXXXXXXXX";
		String userAdminName = args[4]; // "nom.prenom@provider.fr";
		String userAdminPassword = args[5]; // "YYYYYYYYYYYY";

		String filename = args[6]; // "C:\\Users\\sebbi\\eclipse-workspace\\userloader\\src\\main\\resources\\Scopeli_Fichier_Unique_cooperateurs_2022.csv";

		KeycloakUtils keycloakutils = new KeycloakUtils(serverUrl, realm, clientId, clientSecret, userAdminName,
				userAdminPassword);
		int nbAccounts = keycloakutils.getUsersResource().count();
		LOGGER.info("Nombres de comptes déclarés dans le realm Scopéli : " + nbAccounts);

		//////////////////// Changer ici pour tester ou pas
		boolean fake = true;
		////////////////////
		
		//////////////////// Changer ici pour envoyer un email à l'utilisateur
		boolean sendMail = false;
		////////////////////
							
		String fakeString = (fake) ? "FAKE->" : "";

		//CSVReader reader = new CSVReader(,';');
		List<CoopCSV> beans = new CsvToBeanBuilder<CoopCSV>(new InputStreamReader(new FileInputStream(filename), "UTF-8")).withType(CoopCSV.class)
				.withSeparator(';').withSkipLines(1).withIgnoreEmptyLine(true).withQuoteChar('"').build().parse();

		beans.forEach(coop -> {
			LOGGER.debug("Traitement du cooperateur {}", coop);

			if (coop.getMail() != null && !"".equals(coop.getMail())) {

				// Recherche sur le UserName et Matricule
				UserRepresentation founded;
				try {
					founded = keycloakutils.findByUserNameOrMatricule(coop.getMail(), coop.getMatriculeCoop());

					if (founded != null) {

						LOGGER.info("{}Utilisateur {} ({}) trouvé -> MAJ", fakeString, founded.getUsername(),
								founded.getId());

						//Mise é jour des infos de l'utilisateur
						LOGGER_CR.info("{};{};{}MAJ", founded.getUsername(), founded.getId(), fakeString);
						keycloakutils.updateUser(founded, coop, fake);

						// Désactivation si nécessaire
						if (!"actif".equals(coop.getStatut().trim().toLowerCase())) {
							LOGGER_CR.info("{};{};{}Desactivation", founded.getUsername(), founded.getId(), fakeString);
							keycloakutils.desactiveUser(founded, coop, fake);
						}
						
						// Liste des utilisateurs dont l'email est différents dans Keycloak
						if (! founded.getUsername().equals(coop.getMail())) {
							LOGGER_WARNING.info("{};{};{};{};{}",coop.getMatriculeCoop(),coop.getPrenom(),coop.getNom(),coop.getMail(),founded.getUsername());
						}
						
											
					} else {

						if ("actif".equals(coop.getStatut().trim().toLowerCase())) {
							LOGGER.info("{}Utilisateur {} non trouvé -> Création", fakeString, coop.getMail());
							
							try {
								keycloakutils.createUser(coop, fake, sendMail);
								LOGGER_CR.info("{};;{}Création", coop.getMail(), fakeString);
							}catch(WebApplicationException e) {
								LOGGER.error("{}Utilisateur {} non créé", fakeString, coop.getMail());
								
								// Conflit
								if(409 == e.getResponse().getStatus()) {
									LOGGER_ERROR.info("{} {};Utilisateur {} non trouvé mais création impossible pour cause de Mail déjà utilisé ({},{})",
											fakeString, coop.getMail(), coop.getMail(),coop.getPrenom(),coop.getPrenom());
								
								} else {
									LOGGER_ERROR.info("{} {};Utilisateur {} non trouvé mais création impossible pour cause inconnue : {}",
											fakeString, coop.getMail(), coop.getMail(), e.getMessage());
								}
							}
						} else {
							LOGGER.info("{}Utilisateur {} non trouvé mais statut ({}) non attendu", fakeString,
									coop.getMail(), coop.getStatut());
							LOGGER_ERROR.info("{} {};Utilisateur {} non trouvé mais statut ({}) non attendu",
									fakeString, coop.getMail(), coop.getMail(), coop.getStatut());
						}
					}
				} catch (InvalidMatriculeException e) {
					LOGGER.warn("{}Utilisateur {} trouvé par email, mais matricule incorrect (Fichier : {} - Keycloak : {})", fakeString,
							coop.getMail(), coop.getMatriculeCoop(), e.foundedMatricule);
					LOGGER_ERROR.info("{} {};Utilisateur {} trouvé par email, mais matricule incorrect (Fichier : {} ; Keycloak : {})",
							fakeString, coop.getMail(), coop.getMail(), coop.getMatriculeCoop(), e.foundedMatricule);
			
				}
				
				if (!coop.getMatriculeDuo().isBlank()) {
					LOGGER_DUO.info("{};{};{};{};{}",coop.getMail(),coop.getPrenom(),coop.getNom(),coop.getMatriculeCoop(),coop.getMatriculeDuo());
				}

			} else {
				LOGGER.warn("{}Utilisateur de matricule Kaso {} non trouvé", fakeString, coop.getMatriculeKaso());
			}

		});
	}
}
