package fr.scopeli.keycloakhelper.userloader;

public class InvalidMatriculeException extends Exception {
	
	public String foundedMatricule;

	public InvalidMatriculeException() {
		// TODO Auto-generated constructor stub
	}

	public InvalidMatriculeException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidMatriculeException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidMatriculeException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidMatriculeException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
