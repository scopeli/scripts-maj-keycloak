# scripts-maj-keycloak

## Disclaimer

This code source was to make some data initialization and are nor maintenance scripts.

This code is not really beautiful but works.

Be kind with style :)


## Requirement

* Java 21 must be installed (cf https://adoptium.net/fr/temurin/releases/)
* Be carefull when pull Repository. Check that your Eclipse Workspace Encoding is UTF-8 (Project > Properties > Ressource)


## How to Use IT

### Step 1 : Configure keycloak

With Realm Admin Right, declare a new client  "fr.scopeli.keycloahelper.userloader".
The export or our client config is in "config-keycloak" folder

### Step 2 : Build Project

Build maven project

### Step 3 : Use Eclipse "Run As" to Execute Script with parameters

__BE CAREFULL__ : To avoid trouble, a "fake mode" exists in some scripts. This fake mode is set to true by default. Change it to really execute operations. 

Find in class the following code :

```java
		//////////////////// Changer ici pour tester ou pas
		boolean fake = true;
		////////////////////
```

#### Add 3 default groups to all Users :

* __Class__ : fr.scopeli.keycloakhelper.userloader.AddDefaultGroupOldUsers
* __Parameters__ :
    * [0] : Keycloak URL
    * [1] : Realm name
    * [2] : client ID (created in step 1)
    * [3] : The associated client Secret
    * [4] : user Login, that have right to manage user (admin for example)
    * [5] : user password
* __Example__ : _https://annuaire.scopeli.fr/auth realmname fr.scopeli.keycloahelper.userloader passwordClient user@mail.fr UserPa$$w0rd!_

#### Add groups to Users from test file :

* __Class__ : fr.scopeli.keycloakhelper.userloader.InitFormationCoop
* __Parameters__ :
    * [0] : Keycloak URL
    * [1] : Realm name
    * [2] : client ID (created in step 1)
    * [3] : The associated client Secret
    * [4] : user Login, that have right to manage user (admin for example)
    * [5] : user password
    * [6] : source file path
* __Example__ : _https://annuaire.scopeli.fr/auth realmname fr.scopeli.keycloahelper.userloader passwordClient user@mail.fr UserPa$$w0rd! C:\\Users\\sebbi\\eclipse-workspace\\scripts-maj-keycloak\\src\\main\\resources\\CompetencesCooperateursFake.csv_

#### Create user or upate properties of all users :

* __Class__ : fr.scopeli.keycloakhelper.userloader.RefeshCoop
* __Parameters__ :
    * [0] : Keycloak URL
    * [1] : Realm name
    * [2] : client ID (created in step 1)
    * [3] : The associated client Secret
    * [4] : user Login, that have right to manage user (admin for example)
    * [5] : user password
    * [6] : source file path
* __Example__ : _https://annuaire.scopeli.fr/auth realmname fr.scopeli.keycloahelper.userloader passwordClient user@mail.fr UserPa$$w0rd! C:\\Users\\sebbi\\eclipse-workspace\\scripts-maj-keycloak\\src\\main\\resources\\Scopeli_Fichier_Unique_cooperateurs_2022_test.csv _

__BE CAREFULL__ : The CSV must obey the following constraints :
* UTF-8 encoding
* ; as field separator
* " as String Delimiter

To be sure of what is done, I generate the CSV with Libre-Office that allow to specify all these elements (Save as > Select csv file > tick "Edit filter parameter" then Save)
